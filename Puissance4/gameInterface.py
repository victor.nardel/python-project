import pygame, sys
from pygame.locals import *
from game import Game
from player import Player

class GameInterface:
    def __init__(self, P_player: Player, P_equality: bool) -> None:
        self._winner: Player = P_player
        self._equality: bool = P_equality
        
    def RunGame():
        pygame.display.set_caption('Puissance 4 - Game')
        screen = pygame.display.set_mode((700, 600), 0, 32) 
        screen.fill((201, 215, 217))

        for i in range(7):
            pygame.draw.line(screen, (0, 0, 0), (i*100, 0), (i*100, 600))
            if i < 6:
                pygame.draw.line(screen, (0, 0, 0), (0, i*100), (700, i*100))

        pygame.display.update()

        game = Game()
        game.addPlayer(Player(1, 1, (53, 137, 158), "Puissance Bleu"))
        game.addPlayer(Player(2, -1, (148, 30, 16), "Puissance Rouge"))

        while True:
            # ------------- PyGame Event Interface
            for event in pygame.event.get():
                # Close app
                if event.type == QUIT \
                    or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    pygame.quit()
                    sys.exit()
                
                # ------------- Player Actions
                if event.type == MOUSEBUTTONUP:
                    pos = pygame.mouse.get_pos()
                    
                    x: int = 0
                    # Return the grid from verticaly to horizontaly (abstract)
                    for x in range(len(game.getGrid())):
                        if pos[0] >= x * 100 and pos[0] <= (x + 1) * 100:
                            location: tuple =  game.addToken(int(x))
                            if location != None:
                                pygame.draw.ellipse(
                                    screen, 
                                    game.getCurrentPlayer().getColor(),
                                    (location[0] * 100, 500 - location[1] * 100, 100, 100)
                                )
                                pygame.display.flip()

                    GameInterface._winner = game.getWinner()
                    if GameInterface._winner.getId() != 0:
                        print(f'------------ WINNER : {GameInterface._winner.getName()} ------------')
                        GameInterface._equality = False
                        GameInterface.Result()
                    if game.Equality():
                        GameInterface._equality = True
                        GameInterface.Result()


    def Result():
        pygame.display.set_caption('Puissance 4 - Result')
        screen = pygame.display.set_mode((1024, 550), 0, 32)
        background = pygame.image.load('banner.png').convert()
        screen.blit(background, (0, 0))

        font = pygame.font.Font(None, 30)
        if GameInterface._equality:
            text = font.render('Aucune puissance ne gagne la partie', True, (255, 255, 255))
        else:
            text = font.render(f'La {GameInterface._winner.getName()} gagne la partie', True, GameInterface._winner.getColor())

        text_surf = pygame.Surface((text.get_width() + 20, text.get_height() + 20))
        text_surf.fill((255, 255, 255))
        text_surf.blit(text, (10, 10))

        text_x = (screen.get_width() - text_surf.get_rect().width) / 2
        text_y = int(screen.get_height() * 0.85) - 125
        screen.blit(text_surf, (text_x, text_y))

        btn = pygame.Rect(0, 0, 100, 50)
        btn.centerx = screen.get_rect().centerx
        btn.bottom = int(screen.get_height() * 0.85)
        pygame.draw.rect(
            screen,
            GameInterface._winner.getColor(),
            btn,
            border_radius = 5
        )

        font = pygame.font.Font(None, 20)
        text = font.render('Rejouer', True, (254, 254, 254))
        text_rect = text.get_rect()
        text_rect.center = btn.center
        screen.blit(text, text_rect)

        pygame.display.update()

        while True:
            for event in pygame.event.get():
                if event.type == QUIT \
                    or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    pygame.quit()
                    sys.exit()

                x, y = pygame.mouse.get_pos()
                if btn.collidepoint((x, y)):
                    if event.type == MOUSEBUTTONUP:
                        GameInterface.RunGame()