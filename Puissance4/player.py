class Player:
    def __init__(self, P_id: int, P_idGame, P_couleur, P_name: str = "") -> None:
        self._id: int = P_id
        self._inputId: int = P_idGame
        self._couleur = P_couleur
        self._name = P_name
        if not P_name:
            self._name: str = f"Guest{P_id}"

    def getId(self) -> int:
        return self._id
    
    def getInputId(self) -> int:
        return self._inputId

    def getName(self) -> str:
        return self._name

    def getColor(self):
        return self._couleur

    def setColor(self, P_couleur) -> None:
        self._couleur = P_couleur

    def getWebsocket(self):
        return self._websocket

    def setWebsocket(self, P_websocket) -> None:
        self._websocket = P_websocket