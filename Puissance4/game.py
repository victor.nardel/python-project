from player import Player

class Game:
    def __init__(self) -> None:
        self._players: list[Player] = []
        # The most simple 2 dimensional table declaration I've found
        self._grid = [[0 for i in range(6)] for j in range(7)]
        self._round: int = 0
        self._currentPLayer: Player = Player(0, 0, (0, 0, 0))
        self._winner: Player = Player(0, 0, (0, 0, 0))

    def addPlayer(self, P_player: Player) -> None:
        self._players.append(P_player)

    def getPlayers(self) -> list[Player]:
        return self._players

    def getCurrentPlayer(self) -> Player:
        return self._currentPLayer

    def getGrid(self):
        return self._grid
    
    def getWinner(self) -> Player:
        return self._winner

    def addToken(self, x: int):
        # Using the Modulo to simplify the round by round cuz there are only 2 players
        if self._round % 2 == 0:
            self._currentPLayer = self._players[0]
        else:
            self._currentPLayer = self._players[1]
        self._round += 1

        for y in range(len(self._grid[x])):
            if y <= 6 and self._grid[x][y] == 0:
                self._grid[x][y] = self._currentPLayer.getInputId()
                self.testWinVerticals(x, y - 3, self._currentPLayer)
                self.testWinHorizontals(x - 3, y, self._currentPLayer)
                self.testWinLeftDiagonals(x - 3, y - 3, self._currentPLayer)
                self.testWinRightDiagonals(x + 3, y - 3, self._currentPLayer)
                self.show()
                return x, y
    
    def Equality(self) -> bool:
        if self._round >= 42:
            return True

    def testWinVerticals(self, x: int, y: int, P_player: Player) -> None:
        if y >= 0:
            if self._grid[x][y] + self._grid[x][y + 1] + self._grid[x][y + 2] + \
            self._grid[x][y + 3] == P_player.getInputId() * 4:
                self._winner = P_player

    def testWinHorizontals(self, x: int, y: int, P_player: Player) -> None:
        if x >= 0:
            if self._grid[x][y] + self._grid[x + 1][y] + self._grid[x + 2][y] + \
            self._grid[x + 3][y] == P_player.getInputId() * 4:
                self._winner = P_player

    def testWinLeftDiagonals(self, x: int, y: int, P_player: Player) -> None:
        if x >= 0 and y >= 0:
            if self._grid[x][y] + self._grid[x + 1][y + 1] + self._grid[x + 2][y + 2] + \
            self._grid[x + 3][y + 3] == P_player.getInputId() * 4:
                self._winner = P_player

    def testWinRightDiagonals(self, x: int, y: int, P_player: Player) -> None:
        if x <= 6 and y >= 0:
            if self._grid[x][y] + self._grid[x - 1][y + 1] + self._grid[x - 2][y + 2] + \
            self._grid[x - 3][y + 3] == P_player.getInputId() * 4:
                self._winner = P_player

    def show(self) -> None:
        print()
        for x in range(len(self._grid)):
            for y in range(len(self._grid[x])):
                print(self._grid[x][y], end='  ')
            print()
    
    # ------------- Game test in consol
    def run(self) -> None:
        self.addPlayer(Player(1, 1, (255, 211, 0), "Yellow"))
        self.addPlayer(Player(2, -1, (255, 0, 0), "Red"))

        while True:
            print()
            print()
            x = input("n° de colonne : ")
            if x.strip().isdigit() and int(x) >= 0 and int(x) <= 6:
                self.addToken(int(x))
                self.show()
            else:
                print("INVALID ENTRY")

# p = Game()
# p.run()