# ![Puissance4](./banner.png "Puissance 4 Logo")

## GOP4 - Game Of Puissance 4

Ce projet consiste en une implémentation du jeu Puissance 4 en Python 3.10, utilisant la bibliothèque Pygame pour l'interface graphique.

## Introduction

Il était une fois deux puissances en guerre, la puissance Rouge et la puissance Bleue, dans un monde médiéval où la magie était présente partout.

Pour décider du sort de la guerre, les deux puissances décidèrent de s'affronter dans un jeu appelé Puissance 4. Le jeu se jouait sur un plateau de jeu divisé en 6 lignes et 7 colonnes, pour un total de 42 emplacements pour les jetons.

Les joueurs devaient placer leurs jetons de couleur respective sur le plateau en alternant jusqu'à ce que l'un d'entre eux parvienne à aligner 4 jetons de sa couleur horizontalement, verticalement ou en diagonale. La première puissance à réaliser cet alignement remportait la partie et la guerre.

Cependant, si tous les jetons étaient joués sans qu'aucun alignement ne soit réalisé, la partie était déclarée en égalité, et la guerre devait se poursuivre.

## Sommaire

- [Dépendances et environnement virtuel](#dépendances-et-environnement-virtuel)
- [Démarrage](#démarrage)
- [Fonctionnalités](#fonctionnalités)
- [Documentation](#documentation)
- [Auteur](#auteur)

## Dépendances et environnement virtuel

Python 3.10 doit être installé sur votre machine.

Pour générer l'environnement virtuel, exécutez la commande suivante à la racine du projet :

```sh
py -m venv .env
```

Pour activer l'environnement virtuel, exécutez la commande suivante à la racine du projet :

Sous Windows :

```sh
.env\Scripts\activate.bat
```

Sous Linux/MacOS :

```sh
source .env/bin/activate
```

Pour installer les dépendances, exécutez la commande suivante à la racine du projet :

```sh
pip install -r requirements.txt
```

Si Pygame génère une erreur à l'installation, vous pouvez essayer d'installer une version préliminaire avec la commande suivante :

```sh
pip install pygame --pre
```

## Démarrage

Pour démarrer le jeu Puissance 4, exécutez la commande suivante à la racine du projet :

```sh
python .\Puissance4
```

Ou depuis le package Puissance4 :

```sh
python __main__.py
```

Vous pouvez aussi lancer le jeu en console en décommentant les 2 dernières lignes du fichier game.py.

## Fonctionnalités

Le jeu implémente les fonctionnalités suivantes :

- Interface graphique en utilisant Pygame.
- Possibilité de jouer contre un autre joueur humain.
- Possibilité de recommencer une partie.

## Documentation

- [Users Stories](Documentation/documentation.md)
- [Règles du jeu](Documentation/reglement.md)

## Auteur

Le projet a été développé par Victor Nardella & Yann Morin.
