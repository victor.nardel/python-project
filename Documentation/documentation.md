# Réalisation d'un serveur de jeux Puissance 4

## User stories

En tant que joueur de Puissance 4, je souhaite :

- pouvoir quitter une partie en cours.
- que la partie se termine lorsqu'un joueur aligne 4 jetons de sa couleur en diagonale, horizontalement ou verticalement.
- que la partie soit déclarée en égalité si aucun pion n'est possible d'être ajouté et que personne n'a aligné ses 4 jetons en premier.

## Fonctionnalités du front-end en Python

Le front-end en Python doit permettre :

- la création d'un plateau de jeu, avec une interface utilisateur claire et intuitive.
- l'animation d'un jeton placé par un joueur, de manière à ce que l'utilisateur puisse facilement suivre la progression de la partie.
- l'affichage d'un écran de victoire lorsque l'un des joueurs a aligné 4 jetons de sa couleur.
- l'affichage d'un écran d'égalité lorsque la partie est déclarée nulle.
- le retour console de la partie en cours, avec des informations détaillées sur les mouvements des joueurs et l'état actuel du plateau de jeu.
