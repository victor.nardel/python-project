# Règles du jeu

## Définition du plateau de jeu

- Plateau de jeu avec 42 emplacements pour les jetons répartis en 6 lignes et 7 colonnes.
- 42 jetons max de 2 couleurs différentes.
- 2 joueurs min et max.

## Commencer une partie de jeu

Pour commencer une partie, chaque joueur doit placer un jeton de sa couleur dans l'une des colonnes du plateau de jeu. Le jeton apparaît alors en bas de la colonne. Les joueurs alternent ensuite jusqu'à ce que l'un d'entre eux aligne 4 jetons de sa couleur horizontalement, verticalement ou en diagonale.

## Comment gagner une partie de jeu

Pour gagner une partie, il suffit d’être le premier à aligner 4 jetons de sa couleur horizontalement, verticalement ou diagonalement.

Si lors d’une partie, tous les jetons sont joués sans qu’il y ait d’alignement de jetons, la partie est déclarée en égalité.
